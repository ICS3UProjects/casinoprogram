# The Gold Coin Casino
A multi-purpose, efficient, and entertaining Casino Program made using Java and the Eclipse IDE. 

<Add image here>

## Setting Up The Program:

1. Download the zip file of the repository or copy paste *main.java* into an IDE of your choice. 
2. Run the Java program

### Optional: Setting Up The Music

1. Identify the file path that your Java project is in
2. Use any file manager to navigate to the above file path
3. Add casinomusic.wav under the main project folder
4. Rerun the Java program

